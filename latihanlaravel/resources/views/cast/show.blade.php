@extends('layout.master')

@section('judul')
Halaman Detail Cast
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<h4>{{$cast->umur . " tahun"}}</h4>
<p>{{$cast->bio}}</p>

@endsection