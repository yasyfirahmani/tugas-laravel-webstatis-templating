@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <form action="/welcome" method="post">
    @csrf
    <h2>Buat Account Baru</h2>   
    <h4>Sign Up Form</h4>
    <form action="welcome.html" method="get">
        <label for="fname">First name :</label><br><br>
        <input type="text"><br><br>
        <label for="lname">Last name :</label><br><br>
        <input type="text"><br><br>
        <label for="gender">Gender</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br><br>
        <label for="nationality">Nationality</label><br><br>
        <select name="nationality" id="">
            <option value="1">indonesia</option>
            <option value="2">wakanda</option>
            <option value="3">land of dawn</option>
            <option value="4">banjaran</option>
        </select><br><br>
        <label for="language-spoken">Language Spoken</label><br><br>
        <input type="checkbox" name="language-spoken" id="">Bahasa indonesia <br>
        <input type="checkbox" name="language-spoken" id="">English <br>
        <input type="checkbox" name="language-spoken" id="">others <br><br>
        <label for="bio">Bio</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up"> 
   
    </form>

@endsection