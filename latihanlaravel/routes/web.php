<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

 Route::get('/form', 'AuthController@bio');

 Route::post('/welcome', 'AuthController@kirim');

 Route::get('/data-table', function(){
     return view('table.data-table');
 });

 //CRUD Cast
 //create

 Route::get('/cast/create', 'CastController@create');//	menampilkan form untuk membuat data pemain film baru
 Route::post('/cast', 'CastController@store');//menyimpan data baru ke tabel Cast

 //read
 Route::get('/cast', 'CastController@index');//menampilkan list data para pemain film 
 Route::get('/cast/{kategori_id}', 'CastController@show');//menampilkan detail data pemain film dengan id tertentu

 //update
 Route::get('/cast/{cast_id}/edit', 'CastController@edit');//menampilkan form untuk edit pemain film dengan id tertentu
 Route::put('/cast/{cast_id}', 'CastController@update');//	menyimpan perubahan data pemain film (update) untuk id tertentu

 //delete
 Route::delete('/cast/{cast_id}', 'CastController@destroy');//	menghapus data pemain film dengan id tertentu