<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function index()
    {
        $cast = Cast::all();

        return view('cast.index', compact('cast'));
    }
    public function create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:255',
        ]);
        $cast = new Cast;

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();

        return redirect('/cast');
    }
    public function show($id)
    {
        $cast = Cast::find($id)->first();

        return view('cast.show', compact('cast'));
    }
    public function edit($id)
    {
        $cast = Cast::find($id);

        return view('cast.edit', compact('cast'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required|max:255',
        ]
    );
    $cast = Cast::find($id);

    $cast->nama = $request['nama'];
    $cast->umur = $request['umur'];
    $cast->bio = $request['bio'];

    $cast->save();
    return redirect('/cast');
    }
    public function destroy($id)
    {
        $cast = Cast::find($id);

        $cast->delete();

        return redirect('/cast');
    }
}